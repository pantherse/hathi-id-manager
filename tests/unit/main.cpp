/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gflags/gflags.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <log4cplus/configurator.h>

DEFINE_string(dbhost, "localhost", "DB server host");
DEFINE_int32(dbport, 5432, "DB server port");
DEFINE_string(dbuser, "hathi_unittest", "DB login");
DEFINE_string(dbname, "hathi_id_mgr_unittest", "Database name");
DEFINE_string(logconfig, "", "Loging config file to pass to log4cplus");

using namespace log4cplus;

int main(int argc, char **argv)
{
    ::testing::InitGoogleMock(&argc, argv);
    gflags::ParseCommandLineFlags(&argc, &argv, true);

    log4cplus::initialize();
    if(FLAGS_logconfig != "")
        PropertyConfigurator::doConfigure(FLAGS_logconfig);
    else
        BasicConfigurator::doConfigure();

    return RUN_ALL_TESTS();
}
