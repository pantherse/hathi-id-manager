/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <optional>

#include <gmock/gmock.h>

#include "db.h"

namespace hathi_id_manager
{

class MockDB : public DB
{
public:
    MOCK_METHOD0(connect, void());
    MOCK_CONST_METHOD4(addActor, void(
        const std::string &name,
        const std::string &hashedPass,
        const std::string &salt,
        const unsigned char &hashMode));

    MOCK_CONST_METHOD1(getActorPass,
        const std::optional<const password>(const std::string &name)
    );

    MOCK_CONST_METHOD4(saveActorPass,
        void(const std::string &name,
            const std::string &hashedPass,
            const std::string &salt,
            const unsigned char &hashMode
        )
    );

    MOCK_CONST_METHOD2(checkSession,
        std::optional<std::string>(const std::string &session,
        const unsigned int &timeout
        )
    );

    MOCK_CONST_METHOD2(saveSession,
        const bool(
            const std::string &username,
            const std::string &session
        )
    );

    MOCK_CONST_METHOD1(deleteSession,
        const bool(const std::string &session)
    );

    MOCK_CONST_METHOD1(deleteActor,
        const bool(const std::string &username)
    );
};

} //namespace
