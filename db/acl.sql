CREATE ROLE hathi_id_mgr_session;
GRANT SELECT ON hathi_actors TO hathi_id_mgr_session;
GRANT SELECT, INSERT, UPDATE, DELETE ON hathi_sessions TO hathi_id_mgr_session;

CREATE ROLE hathi_id_mgr_usermgt;
GRANT DELETE ON hathi_sessions TO hathi_id_mgr_usermgt;
GRANT SELECT, INSERT, UPDATE, DELETE ON hathi_actors TO hathi_id_mgr_usermgt;
