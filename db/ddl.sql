CREATE TABLE IF NOT EXISTS hathi_actors (
    name VARCHAR(255) CONSTRAINT hathi_user_pkey PRIMARY KEY,
    salt BYTEA,
    hashedpass BYTEA,
    hashmode SMALLINT
);

CREATE TABLE IF NOT EXISTS hathi_sessions (
    name VARCHAR(255) UNIQUE NOT NULL
        REFERENCES hathi_actors(name) ON DELETE CASCADE ON UPDATE CASCADE,
    session BYTEA UNIQUE NOT NULL,
    lastused TIMESTAMP NOT NULL DEFAULT NOW(),
    CONSTRAINT hathi_sessions_key PRIMARY KEY (session, name),
    CONSTRAINT hathi_sessions_name_key UNIQUE(name)
);
CREATE INDEX CONCURRENTLY IF NOT EXISTS session_time ON hathi_sessions(lastused);
