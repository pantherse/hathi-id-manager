/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <list>
#include <mutex>
#include <condition_variable>
#include <string>
#include <utility>
#include <memory>
#include <functional>
#include <sstream>
#include <iomanip>

#include <log4cplus/logger.h>

#include <jsoncpp/json/value.h>

#include "gtest/gtest_prod.h"

#include "socketinfo.h"
#include "password.h"
#include "db.h"

namespace hathi_id_manager
{

///
/// Handle client connections
///
class ClientHandler
{

public:
    ///
    /// Default constructor
    ///
    ClientHandler(std::function<std::unique_ptr<DB>()> initDBConn) :
        logger(log4cplus::Logger::getInstance("ClientHandler")),
        initDBConn(initDBConn)
    {}

    ///
    /// Delete all the copy/move constructors/operators
    ///
    ClientHandler(const ClientHandler &) = delete;
    ClientHandler(ClientHandler &&) = delete;

    ClientHandler operator=(ClientHandler) = delete;
    ClientHandler operator=(ClientHandler &&) = delete;

    virtual ~ClientHandler() {}

    void addClient(SocketInfo && sockParam);

private:
    log4cplus::Logger logger;

    std::list<SocketInfo> clients;
    bool keepRunning = true;

    std::mutex syncLock;
    std::condition_variable cv;
    std::function<std::unique_ptr<DB>()> initDBConn;

    ///
    /// Generate a session key
    ///
    const std::string genSession() const;

    ///
    /// Hex-encode data
    ///
    const std::string hexEncode(const std::string &data) const;

    ///
    /// Hex-decode data
    ///
    const std::string hexDecode(const std::string &data) const;

    ///
    /// Get the session value from the request
    ///
    /// Validate that the session field is present and of the correct type.
    /// If it fails validation, the "error" will be placed in retVal contaning
    /// the cause of the error
    ///
    /// \param inVal JSON message to get the session from
    /// \param retVal JSON object to place response message in
    /// \return decoded session if present and of valid type in inVal.
    ///
    const std::optional<std::string> getSessionToken(const Json::Value &inVal,
        Json::Value &retVal) const;

    ///
    /// Get the actor value from the request
    ///
    /// Validate that the actor field is present and of the correct type.
    /// If it fails validation, the "error" will be placed in retVal contaning
    /// the cause of the error
    ///
    /// \param inVal JSON message to get the actor from
    /// \param retVal JSON object to place response message in
    /// \return actor value if present and of valid type in inVal.
    ///
    const std::optional<std::string> getActor(const Json::Value &inVal,
        Json::Value &retVal) const;

    ///
    /// Function to listen to client messages
    ///
    /// \param sockParam Client socket connection
    ///
    void handleClient(SocketInfo && sockParam);

    ///
    /// Verifies if client login info is valid
    ///
    /// \return true if username/password provided and stored matches. False otherwise
    /// \param dBConn DB instance to get stored password from
    /// \param passObj Password object instance
    /// \param inVal JSON message received
    /// \param retVal JSON object to place response message in
    ///
    void handleLogin(DB &dbConn, Password &passObj, 
        const Json::Value &inVal, Json::Value &retVal);

    ///
    /// Save a new actor
    ///
    /// \param dBConn DB instance
    /// \param passObj Password object instance
    /// \param inVal JSON message received
    /// \param retVal JSON object to place response message in
    ///
    void handleNewActor(DB &dbConn, Password &passObj,
        const Json::Value &inVal, Json::Value &retVal);

    ///
    /// Delete an actor
    ///
    /// \param dBConn DB instance
    /// \param inVal JSON message received
    /// \param retVal JSON object to place response message in
    ///
    void handleDeleteActor(DB &dbConn,
        const Json::Value &inVal, Json::Value &retVal);

    ///
    /// Logout a user
    ///
    /// \param dBConn DB instance
    /// \param inVal JSON message received
    /// \param retVal JSON object to place response message in
    ///
    void handleLogout(DB &dbConn, const Json::Value &inVal,
        Json::Value &retVal);

    ///
    /// Check if session valid and for associated actor
    ///
    /// \param dbConn DB instance
    /// \param inVal JSON message received
    /// \param retVal JSON object to place response message in
    ///
    void handleCheckSession(DB &dbConn, const Json::Value &inVal,
        Json::Value &retVal);

    // Test case declarations
    FRIEND_TEST(ClientHandlerTest, addClient);

	FRIEND_TEST(ClientHandlerTest, checkCommonFieldsFunctionMissing);
	FRIEND_TEST(ClientHandlerTest, checkCommonFieldsVersionMissing);
	FRIEND_TEST(ClientHandlerTest, checkCommonFieldsWrongVersion);
	FRIEND_TEST(ClientHandlerTest, checkCommonFieldsRequestMissing);
	FRIEND_TEST(ClientHandlerTest, checkCommonFieldsReqeustWrongType);
	FRIEND_TEST(ClientHandlerTest, checkCommonFieldsNotImplemented);

	FRIEND_TEST(ClientHandlerTest, getSessionTokenNoSession);
	FRIEND_TEST(ClientHandlerTest, getSessionTokenWrongType);
	FRIEND_TEST(ClientHandlerTest, getSessionTokenValidData);

	FRIEND_TEST(ClientHandlerTest, getActorNoSession);
	FRIEND_TEST(ClientHandlerTest, getActorWrongType);
	FRIEND_TEST(ClientHandlerTest, getActorValidData);

    FRIEND_TEST(ClientHandlerTest, handleLoginHandleClient);
    FRIEND_TEST(ClientHandlerTest, handleLoginNoUsername);
    FRIEND_TEST(ClientHandlerTest, handleLoginUsernameWrongType);
    FRIEND_TEST(ClientHandlerTest, handleLoginNoPassword);
    FRIEND_TEST(ClientHandlerTest, handleLoginPasswordWrongType);
    FRIEND_TEST(ClientHandlerTest, handleLoginValidCredential);
    FRIEND_TEST(ClientHandlerTest, handleLoginSessionSaveFail);
    FRIEND_TEST(ClientHandlerTest, handleLoginInvalidUser);
    FRIEND_TEST(ClientHandlerTest, handleLoginInvalidPassword);

    FRIEND_TEST(ClientHandlerTest, handleNewActorHandleClient);
    FRIEND_TEST(ClientHandlerTest, handleNewActorNoPassword);
    FRIEND_TEST(ClientHandlerTest, handleNewActorPasswordWrongType);
    FRIEND_TEST(ClientHandlerTest, handleNewActorUserAdded);
    FRIEND_TEST(ClientHandlerTest, handleNewActorUserAddFail);

    FRIEND_TEST(ClientHandlerTest, hexDecodeTest);

	FRIEND_TEST(ClientHandlerTest, handleDeleteActorHandleClient);
	FRIEND_TEST(ClientHandlerTest, handleDeleteActorSessionInvalid);
	FRIEND_TEST(ClientHandlerTest, handleDeleteActorDeleteSuccess);
	FRIEND_TEST(ClientHandlerTest, handleDeleteActorDeleteFail);

	FRIEND_TEST(ClientHandlerTest, handleLogoutHandleClient);
	FRIEND_TEST(ClientHandlerTest, handleLogoutSessionInvalid);
	FRIEND_TEST(ClientHandlerTest, handleLogoutSuccess);
	FRIEND_TEST(ClientHandlerTest, handleLogoutFail);

    FRIEND_TEST(ClientHandlerTest, handleCheckSessionHandleClient);
    FRIEND_TEST(ClientHandlerTest, handleCheckSessionValidSession);
    FRIEND_TEST(ClientHandlerTest, handleCheckSessionInvalidSession);
    FRIEND_TEST(ClientHandlerTest, handleCheckSessionWrongActor);
    FRIEND_TEST(ClientHandlerTest, handleCheckSessionNoAssociatedActors);
};

}
