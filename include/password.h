/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <string>
#include <tuple>
#include <utility>

#include <openssl/evp.h>
#include <openssl/err.h>

#include <log4cplus/logger.h>
#include <log4cplus/loggingmacros.h>

#include "gtest/gtest_prod.h"

namespace hathi_id_manager
{

class Password
{
private:
    /// logger instance
    log4cplus::Logger logger =
        log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("Password"));

    /// Hash mode. To make room for future hash versions
    static const unsigned char defaultHashMode;

    ///
    /// Build the error string with OpenSSL error string
    ///
    /// \param prefix Message to append the OpenSSL error string to
    /// \param code OpenSSL error code (usually used with ERR_get_error())
    ///
    inline const std::string buildErrorString(
        const std::string &prefix, const int &code = ERR_get_error()
    ) const;

    ///
    /// Deleter function for EVP_MD_CTX
    ///
    /// \param md EVP_MD_CTX to free up
    ///
    inline void cleanMDCtx(EVP_MD_CTX *md) const
    {
        LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT(__PRETTY_FUNCTION__)
            << LOG4CPLUS_TEXT(" called"));
        EVP_MD_CTX_free(md);
    }

    /// Digest data
    typedef std::tuple<
        const std::string,  // hash
        const std::string,  // salt
        const unsigned int, // hash size
        const unsigned char // hash mode
    > digest;

    ///
    /// Build the digest data that calcPassHash will return
    ///
    const inline digest buildDigest(
        const std::string &hash,
        const std::string &salt,
        const unsigned int &size,
        const unsigned char &hashMode) const
    {
        return std::make_tuple(hash, salt, size, hashMode);
    }

    ///
    /// Return the digest size
    ///
    /// This is to make the calculation consistent accross the board
    ///
    /// \param hashMode Hash mode to get size for
    ///
    const size_t getMDSize(const unsigned char hashMode = defaultHashMode) const;

    /// Calculate the salted password hash usign the given salt
    ///
    /// \param pass plaintext password
    /// \param salt 256-bit hash
    /// \return salted password hash value
    ///
    const digest calcPassHash(
        const std::string &pass,
        const std::string &salt,
        const unsigned char digestMode = defaultHashMode) const;

public:
    Password() = default;
    Password(const Password &rhs) = default;
    Password(Password &&rhs) = default;
    Password & operator = (const Password & rhs) = default;
    Password & operator = (Password && rhs) = default;

    virtual ~Password(){}

    ///
    /// Calculate the salted password hash using the newly generated salt.
    ///
    /// This is intended to be used for a new password.
    ///
    /// \param pass plaintext password
    /// \return salte password hash value
    ///
    virtual const digest calcPassHash(const std::string &pass) const;

    ///
    /// Get the hash string
    ///
    /// \param d digest object to get value from
    ///
    inline const std::string getHash(const digest &d) const
    {
        return std::get<0>(d);
    }

    ///
    /// Get the salt
    ///
    /// \param d digest object to get value from
    ///
    inline const std::string getSalt(const digest &d) const
    {
        return std::get<1>(d);
    }

    ///
    /// Get the hash size
    ///
    /// \param d digest object to get value from
    ///
    inline const unsigned getSize(const digest &d) const
    {
        return std::get<2>(d);
    }

    ///
    /// Get the hash mode
    ///
    /// \param d digest object to get value from ///
    inline const unsigned char getHashMode(const digest &d) const
    {
        return std::get<3>(d);
    }

    ///
    /// Verify that the user-provided plaintext password matches the stored password
    ///
    /// \param plainTextPass The user-provided password
    /// \param salt The salt used when the password was 1st saved
    /// \param baseHash The hashed password to compare with
    ///
    /// \return whether the plaintext matches the stored password
    ///
    virtual const bool verifyPassword(
        const std::string &plainTextPass,
        const std::string &salt,
        const std::string &baseHash,
        const unsigned char hashMode) const;

	FRIEND_TEST(PasswordTest, calcPassHashsaltedAllBinary);
	FRIEND_TEST(PasswordTest, calcPassHashShortSalt);
	FRIEND_TEST(PasswordTest, calcPassHashWrongVersion);
	FRIEND_TEST(PasswordTest, calcPassHashNoSalt);

    FRIEND_TEST(PasswordTest, getMDSize);

    friend class MockPassword;

};

} //namespace
