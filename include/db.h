/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <tuple>
#include <string>
#include <optional>
#include <vector>

namespace hathi_id_manager
{

///
/// Base class for DB operation
///
class DB
{
public:
    DB() = default;
    DB(const DB &rhs) = default;
    DB(DB &&rhs) = default;
    DB & operator = (const DB & rhs) = default;
    DB & operator = (DB && rhs) = default;

    virtual ~DB(){}

    ///
    /// Connect to the DB server
    ///
    /// Throws DBError if the connection failed
    ///
    virtual void connect() = 0;

    ///
    /// Data structure for saved password
    ///
    /// This should mirror Password::digest; but not use it to reduce coupling
    /// with that class
    ///
    typedef std::tuple<
        std::string,        // hashed password
        std::string,        // salt
        unsigned char       // hash mode
    > password;

    ///
    /// Get the salted hash pass from the password object
    ///
    /// \param obj password object to get the value from
    ///
    inline const std::string getHashedPass(const password &obj)
    {
        return std::get<0>(obj);
    }

    ///
    /// Get the salt from the password object
    ///
    /// \param obj password object to get the value from
    ///
    inline const std::string getSalt(const password &obj)
    {
        return std::get<1>(obj);
    }

    ///
    /// Get the hash mode from the password object
    ///
    /// \param obj password object to get the value from
    ///
    inline const unsigned char getHashMode(const password &obj)
    {
        return std::get<2>(obj);
    }

    ///
    /// Add a new actor
    ///
    virtual void addActor(const std::string &name,
        const std::string &hashedPass, const std::string &salt,
        const unsigned char &hashMode) const = 0;

    ///
    /// Get the stored password info from the DB
    ///
    /// \param username Username to get DB info from
    ///
    /// \return password info if user is found, nullopt otherwise
    ///
    virtual const std::optional<const password> getActorPass(
        const std::string &username) const = 0;

    ///
    /// Check if a session is valid and active.
    ///
    /// If a session is in the DB; but, is expired 
    ///
    /// \param session
    /// \param timeout time, in seconds, session was last used
    /// \return username if session is valid
    ///
    virtual std::optional<std::string> checkSession(
        const std::string &session,
        const unsigned int &timeout) const = 0;

    ///
    /// Save/renew a session
    ///
    /// Basically, do an UPSERT of the username/session token combo. The UPDATE
    /// would be to update the use time
    ///
    /// \param username The username
    /// \param session session token
    ///
    virtual const bool saveSession(
        const std::string &username,
        const std::string &session) const = 0;

    ///
    /// Delete a session
    ///
    /// \param session Session token to delete
    ///
    virtual const bool deleteSession(const std::string &session) const = 0;

    ///
    /// Delete a user
    ///
    /// \param name Actor to delete
    ///
    virtual const bool deleteActor(const std::string &username) const = 0;
};

class DBError : public std::exception
{
private:
    std::string _what;

public:
    DBError(const std::string &what) :
        _what(what)
    {}

    inline const char *what() const noexcept
    {
        return _what.c_str();
    }
};

} //namespace
