/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#pragma once

#include <string>
#include <memory>

#include <log4cplus/logger.h>
#include <openssl/bio.h>

namespace hathi_id_manager
{

///
/// Class to manage sockets
///
class SocketInfo
{

public:
    ///
    /// Default constructor
    ///
    SocketInfo() :
        logger(log4cplus::Logger::getInstance("SocketInfo"))
    {}

    ///
    /// Copy constructor
    ///
    /// \param cpy instance to copy
    ///
    SocketInfo(const SocketInfo &cpy) :
        logger(log4cplus::Logger::getInstance("SocketInfo"))
    {
        sockBio = cpy.sockBio;
    }

    ///
    /// Move constructor
    ///
    /// \param instance to move to this instance
    ///
    SocketInfo(SocketInfo && cpy) :
        logger(log4cplus::Logger::getInstance("SocketInfo")),
        sockBio(std::move(cpy.sockBio))
    {}

    ///
    /// Constructor taking a BIO pointer
    ///
    SocketInfo(BIO* && cpy)
        : logger(log4cplus::Logger::getInstance("SocketInfo"))
    {
        saveBio(cpy);
    }

    ///
    /// assignment operator
    ///
    /// \param rhs right side of the operator
    ///
    SocketInfo &operator = (const SocketInfo &rhs)
    {
        sockBio = rhs.sockBio;
        return *this;
    }

    ///
    /// move assignment operator
    ///
    /// \param rhs right side of the operator
    ///
    SocketInfo &operator = (SocketInfo && rhs)
    {
        logger = std::move(rhs.logger);
        sockBio = std::move(rhs.sockBio);
        return *this;
    }

    ///
    /// Destructor
    ///
    virtual ~SocketInfo()
    {}

    ///
    /// Close the socket connection
    ///
    inline void closeSocket()
    {
        sockBio.reset();
    }

    ///
    /// Get the IP address the socket is connected to
    ///
    virtual const std::string getSocketIP() const;
    
    ///
    /// Attempt to read from client.
    ///
    /// \return Number of bytes received.
    /// \throw runtime_error if an error is encountered during reading
    ///     from the socket
    ///
    virtual const std::string readData() const;

    ///
    /// Attempt to send to client
    ///
    /// \param msg Data to send
    /// \return Number of bytes sent
    /// \throw runtime_error if an error is encountered during writing
    ///     to the socket
    ///
    virtual const size_t writeData(const std::string &msg) const;

    ///
    /// Return the BIO pointer
    ///
    /// \throw logic_error if this isntance's BIO is not set
    ///
    inline std::shared_ptr<BIO> getBio() const
    {
        if(!sockBio)
            throw std::logic_error("Socket not created");
        return sockBio;
    }

    ///
    /// Store the BIO
    ///
    /// \param ptr BIO to store
    ///
    inline void saveBio(BIO *ptr)
    {
        sockBio = std::shared_ptr<BIO>(
            ptr,
            [](BIO *p){ BIO_free_all(p); }
        );
    }

protected:
    void logOpensslError(const std::string &logMsg, log4cplus::Logger logger) const;

private:
    log4cplus::Logger logger;
    std::shared_ptr<BIO> sockBio;
};

} //namespace
