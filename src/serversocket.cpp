/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <sstream>

#include <log4cplus/loggingmacros.h>
#include <openssl/err.h>

#include "serversocket.h"

using namespace std;

namespace hathi_id_manager
{

void ServerSocket::initServer(const unsigned int &port, const std::string &privKey,
    const std::string &cert, const string &host)
{
    LOG4CPLUS_DEBUG(logger, __PRETTY_FUNCTION__<<" start");
    ostringstream hostPort;
    hostPort << host << ":" << port;
    LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Value of hostPort: ") << hostPort.str());
    auto bio = BIO_new_accept(hostPort.str().c_str());
    if(!bio)
    {
        logOpensslError("Failed to allocate accept BIO.", logger);
        throw runtime_error("Failed to allocate accept BIO.");
    }

    if(BIO_set_bind_mode(bio, BIO_BIND_REUSEADDR_IF_UNUSED)!= 1)
    {
        logOpensslError("Failed to set bind mode.", logger);
        throw runtime_error("Failed to set bind mode.");
    }

    // Configure SSL if the keys are provided. Otherwise, it's going to be
    // cleartext
    if(privKey.size() && cert.size())
    {
        LOG4CPLUS_INFO(logger, "Enabling SSL");
        sslCtx = unique_ptr<SSL_CTX, function<void(SSL_CTX *)>>(
            SSL_CTX_new(TLS_server_method()),
            [](SSL_CTX *ptr){ SSL_CTX_free(ptr); }
        );
        if(!sslCtx)
        {
            logOpensslError("Failed to allocate SSL context", logger);
            throw runtime_error("Failed to allocate SSL context");
        }

        auto ctx = sslCtx.get();
        if(!SSL_CTX_use_certificate_chain_file(ctx, cert.c_str()) ||
            !SSL_CTX_use_PrivateKey_file(ctx, privKey.c_str(), SSL_FILETYPE_PEM) ||
            !SSL_CTX_check_private_key(ctx) ||
            !SSL_CTX_set_min_proto_version(ctx, TLS1_2_VERSION)
        )
        {
            logOpensslError("Failed to configure SSL", logger);
            throw runtime_error("Failed to configure SSL");
        }

        auto sbio = BIO_new_ssl(ctx, 0);
        SSL *ssl;
        BIO_get_ssl(sbio, &ssl);
        if(!ssl)
        {
            logOpensslError("Failed to get SSL pointer", logger);
            throw runtime_error("Failed to get SSL pointer");
        }
        SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);

        BIO_set_accept_bios(bio, sbio);
        sslEnabled = true;
    }
    else
        LOG4CPLUS_INFO(logger, "Server listening in cleartext");

    saveBio(bio);
    startListener();
}

void ServerSocket::startListener()
{
    static bool started = false;
    if(started)
        throw logic_error("Already listening for incoming connection");

    started = true;
    if(BIO_do_accept(getBio().get()) < 1)
    {
        logOpensslError("Failed to start listening for incoming connection.", logger);
        throw runtime_error("Failed to start listening for incoming connection.");
    }

    LOG4CPLUS_INFO(logger, "Listening for incoming connection");
}

void ServerSocket::handleConnection(ClientHandler &handler)
{
    auto lBio = getBio();
    auto acptRet = BIO_do_accept(lBio.get());
    LOG4CPLUS_TRACE(logger, LOG4CPLUS_TEXT("Value of acptRet: ") << acptRet);
    if(acptRet == 1)
    {
        LOG4CPLUS_DEBUG(logger, "Received incomming connection");
        auto cbio = BIO_pop(lBio.get());
        SocketInfo clientSocket(move(cbio));

        if(sslEnabled)
        {
            auto ip = clientSocket.getSocketIP();
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Initiate SSL handshake with ") << ip);
            if(BIO_do_handshake(clientSocket.getBio().get()) <= 0)
            {
                ostringstream msg;
                msg << "Handshake with client " << ip << " failed.";
                logOpensslError(msg.str(), logger);
                return;
            }
            else
            {
                LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Handshake with client ") <<
                    ip << " successful"
                );
            }
        }
        else
            LOG4CPLUS_DEBUG(logger, "SSL not enabled");

        LOG4CPLUS_DEBUG(logger, "Start client handling");
        handler.addClient(move(clientSocket));
    }
    else
    {
        if(ERR_peek_error())
        {
            logOpensslError("Failed to accept incoming connection", logger);
            throw runtime_error("Failed to accept incoming connection");
        }
        else
            LOG4CPLUS_DEBUG(logger, "Accept interrupted");
    }
}

} // namespace
