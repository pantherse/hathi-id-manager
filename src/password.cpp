/*
 * Copyright 2018-present Hathi authors
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <memory>
#include <functional>
#include <cstring>

#include <openssl/err.h>
#include <openssl/rand.h>

#include "password.h"

using namespace std;

namespace hathi_id_manager
{

const unsigned char Password::defaultHashMode = 1;

const string Password::buildErrorString(const string &prefix, const int &code) const
{
    char errstr[256];
    ERR_error_string_n(code, errstr, sizeof(errstr));
    ostringstream msg;
    msg << prefix << " Cause: " << errstr;

    LOG4CPLUS_ERROR(logger, msg.str());

    return msg.str();
}

const size_t Password::getMDSize(const unsigned char hashMode) const
{
    size_t retVal;
    if(hashMode == defaultHashMode)
        retVal = EVP_MD_size(EVP_sha256());
    else
    {
        ostringstream msg;
        msg << "Invalid hash mode " << hashMode;
        throw range_error(msg.str());
    }

    return retVal;
}

const Password::digest Password::calcPassHash(
    const string &pass,
    const string &salt,
    const unsigned char hashMode) const
{
    auto mdctx = unique_ptr<EVP_MD_CTX, function<void(EVP_MD_CTX *)>>(
        EVP_MD_CTX_new(), [this](EVP_MD_CTX *p){ this->cleanMDCtx(p); }
    );

    if(!mdctx)
        throw runtime_error(buildErrorString("Failed to allocate MD context."));

    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Initialize MD context"));
    switch(hashMode)
    {
    case 1:
        if(!EVP_DigestInit_ex(mdctx.get(), EVP_sha256(), nullptr))
            throw runtime_error(buildErrorString("Failed to initialize MD context."));
        break;
    default:
        ostringstream msg;
        msg << "Unexpected hash mode " << hashMode;
        throw range_error(msg.str());
    }

    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Feed salt. Length: ") << salt.size());
    if(!EVP_DigestUpdate(mdctx.get(), salt.data(), salt.size()))
        throw runtime_error(buildErrorString("Failed to hash salt."));

    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Feed password. Length: ") << pass.size());
    if(!EVP_DigestUpdate(mdctx.get(), pass.data(), pass.size()))
        throw runtime_error(buildErrorString("Failed to hash password."));

    unsigned int len;
    unsigned char buf[EVP_MAX_MD_SIZE];
    if(!EVP_DigestFinal_ex(mdctx.get(), buf, &len))
        throw runtime_error(buildErrorString("Failed to finalize hashing."));

    // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
    string retStr(reinterpret_cast<const char *>(buf), len);

    return buildDigest(retStr, salt, len, hashMode);
}

const Password::digest Password::calcPassHash(const string &pass) const
{
    RAND_poll();
    const size_t digestSize = getMDSize();
    auto salt = unique_ptr<unsigned char[]>( new unsigned char [digestSize]);
    int i;
    for(i = 0; i< 3; i++)
    {
        if(RAND_bytes(salt.get(), digestSize))
        {
            LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Salt generated on try ") << i);
            break;
        }
        else
            LOG4CPLUS_WARN(logger, LOG4CPLUS_TEXT("Salt generation failed try ") << i);
    }

    if(i==3)
        throw runtime_error("Failed to generate salt");

    // This will always use the default hash mode
    return calcPassHash(
        pass,
        // NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
        string(reinterpret_cast<char *>(salt.get()), digestSize)
    );
}

const bool Password::verifyPassword(
    const string &plainTextPass,
    const string &salt,
    const string &baseHash,
    const unsigned char hashMode) const
{
    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Verify base hash size is correct"));
    auto hashSize = getMDSize(hashMode);
    if(hashSize != baseHash.size())
        throw range_error("Base hash's size doesn't match hash mode's size");

    LOG4CPLUS_DEBUG(logger, LOG4CPLUS_TEXT("Hash provided plaintext password"));
    auto hashedPass = calcPassHash(plainTextPass, salt, hashMode);

    return !memcmp(getHash(hashedPass).data(), baseHash.data(), hashSize);
}

} // namespace

